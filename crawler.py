import requests
from bs4 import BeautifulSoup
import getpass
from consts import login_url, login_api_url, secure_url, headers


EMAIL = input("Enter Email: ")
USERNAME = input("Enter Username: ")
PASSWORD = getpass.getpass("Enter Password: ")


with requests.Session() as s:
    req = s.get(login_api_url)
    response = BeautifulSoup(req.text, "html.parser")
    login_form_data = {"login": EMAIL, "password": PASSWORD}

    # extract hidden inputs
    for form in response.find_all("form"):
        for inp in form.select("input[type=hidden]"):
            login_form_data[inp.get("name")] = inp.get("value")

    login_api_response = s.post(
        login_api_url, data=login_form_data, headers={"referer": login_url}
    )
    secure_project_page = s.get(secure_url, headers=headers)

    sec_proj_content = BeautifulSoup(secure_project_page.text, "html.parser")
    sec_proj_h1 = sec_proj_content.find("title").get_text().strip()

    print("#" * 100)
    print(secure_project_page.status_code)
    print(sec_proj_h1)
    print("#" * 100)

    repos_url = f"https://github.com/{USERNAME}/?tab=repositories"
    repoa_request = s.get(repos_url)
    repos_response = BeautifulSoup(repoa_request.content, "html.parser")

    repos_list = repos_response.find_all("h3", class_="wb-break-all")
    for r in repos_list:
        link = r.find("a")["href"]
        print(f"Repository link: https://github.com/{link}")
